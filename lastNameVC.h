//
//  lastNameVC.h
//  Workingwithprotocols
//
//  Created by Prince Prabhakar on 30/12/15.
//  Copyright © 2015 Prince Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol sampleprotocoldelegate

-(void) setLastName:(NSString *)lastName;

@end


@interface lastNameVC : UIViewController

@property (nonatomic,retain) id delegate;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
- (IBAction)done:(id)sender;

@end
