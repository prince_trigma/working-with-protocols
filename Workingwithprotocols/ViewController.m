//
//  ViewController.m
//  Workingwithprotocols
//
//  Created by Prince Prabhakar on 30/12/15.
//  Copyright © 2015 Prince Prabhakar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    lastNameVC *lastNameViewController = [segue destinationViewController];
    lastNameViewController.delegate = self;
}

-(void)setLastName:(NSString *)lastName
{
    NSString *fName = [NSString stringWithFormat:@"%@ %@",_firstName.text,lastName];
    _fullName.text = fName;
}


@end
