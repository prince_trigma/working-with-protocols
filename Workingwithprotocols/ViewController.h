//
//  ViewController.h
//  Workingwithprotocols
//
//  Created by Prince Prabhakar on 30/12/15.
//  Copyright © 2015 Prince Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "lastNameVC.h"

@interface ViewController : UIViewController<sampleprotocoldelegate>

@property (weak, nonatomic) IBOutlet UITextField *firstName;

@property (weak, nonatomic) IBOutlet UITextField *fullName;

@end

