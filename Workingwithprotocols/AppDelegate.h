//
//  AppDelegate.h
//  Workingwithprotocols
//
//  Created by Prince Prabhakar on 30/12/15.
//  Copyright © 2015 Prince Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

